//
// Created by benjamin angelard on 20/12/2017.
//

#include "TetravexSolver.h"



/** Creer un tableau de tetravex de taille size x size aléatoire en générant
 * une solution **/
void TetravexSolver::generateTetravex(int size)
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            Tetravex nom;
            listTetravex.push_back(nom);
        }
    }
    own_size = size;
    fillTetravex(size);
}
/**
 *      0
 *    1  2
 *     3
 */

/**
 *
 * Genere les tetravex au random qui vont remplir le tableau listTetravex
 */
void TetravexSolver::fillTetravex(int size)
{

    for (int pos = 0; pos < size * size; pos++)
    {

            Tetravex current = listTetravex.at(pos);


            int * tab = current.getTetravex();
            if (pos < size) /** Premiere ligne randomise le top **/
            {
                tab[0] = rand() % 10; // Tete du tetravex osef car premiere ligne
            }
            else { /** Sinon adapte au bot du precedeptn **/
                int * top = listTetravex.at(pos - size).getTetravex();
                tab[0] = top[3]; /* Top de la ligne courante est le bot du pred **/
            }

            if (pos % size  == 0) /* Premier tetravex osef de gauche **/
            {
                tab[1] = rand() % 10;
            }
            else { /** Sinon creation de gauche egale au droite suivant **/
                    int * pred = listTetravex.at(pos - 1).getTetravex();


                    tab[1] = pred[2]; /** Gauche du courant = droite du pred  **/
                }
            tab[2] = rand() % 10; /** Droite osef car fait iteration par iteration en ordre descendant **/
            tab[3] = rand() % 10;

            }
}

void TetravexSolver::printMap()
{
    for (int i = 0; i < own_size; i++) /** A visualiser sur papier pour comprendre, affiche ligne par ligne les tetravex**/
    {
        std::vector<Tetravex> tetravexTMP = std::vector<Tetravex>();
        /** Recupere la liste des tetravex de la ligne courante **/
        for (int j = 0; j < own_size; j++)
            tetravexTMP.push_back(listTetravex.at(i * own_size + j));
        /** Affiche les éléments lignes par ligne **/
        std::cout <<" " << tetravexTMP.at(0).getTetravex()[0];
        for (int k = 1; k < own_size; k++)
        {
            std::cout << "   " << tetravexTMP.at(k).getTetravex()[0]; /** Affiche les head **/
        }
        std::cout << std::endl;
        for (int k = 0; k < own_size - 1; k++)
        {
            std::cout <<  tetravexTMP.at(k).getTetravex()[1] << " " << tetravexTMP.at(k).getTetravex()[2] << " "; /** Affiche les head **/
        }
        std::cout <<  tetravexTMP.at(own_size - 1).getTetravex()[1] << " " << tetravexTMP.at(own_size - 1).getTetravex()[2] ; /** Affiche les head **/
        std::cout << std::endl;
        std::cout <<" " << tetravexTMP.at(0).getTetravex()[3];
        for (int k = 1; k < own_size; k++)
        {
            std::cout << "   " << tetravexTMP.at(k).getTetravex()[3] ; /** Affiche les head **/
        }
        std::cout << std::endl;
    }


}
void TetravexSolver::pretty_printMap()
{


    std::cout << "       =================================           " << std::endl;
    for (int i = 0; i < own_size; i++) /** A visualiser sur papier pour comprendre, affiche ligne par ligne les tetravex**/
    {
        std::vector<Tetravex> tetravexTMP = std::vector<Tetravex>();
        /** Recupere la liste des tetravex de la ligne courante **/
        for (int j = 0; j < own_size; j++)
            tetravexTMP.push_back(listTetravex.at(i * own_size + j));
        /** Affiche les éléments lignes par ligne **/
        for (int k = 0; k < own_size; k++)
        {
            std::cout << "  " << tetravexTMP.at(k).getTetravex()[0] << "   "; /** Affiche les head **/
        }
        std::cout << std::endl;
        for (int k = 0; k < own_size; k++)
        {
            std::cout <<  tetravexTMP.at(k).getTetravex()[1] << "   " << tetravexTMP.at(k).getTetravex()[2] << " "; /** Affiche les head **/
        }
        std::cout << std::endl;
        for (int k = 0; k < own_size; k++)
        {
            std::cout << "  " << tetravexTMP.at(k).getTetravex()[3] <<  "   "; /** Affiche les head **/
        }
        std::cout << std::endl;
    }
    std::cout << "       =================================      "   << std::endl;

}

/** Shuffle le tableau solution généré par fillTetravex afin de tester notre algorithme **/

void TetravexSolver::shuffle()
{
    for (int i = 0; i < own_size * own_size - 1; i++) {
        int pos = rand() % (own_size * own_size - i); // Random remaining position.
        auto temp = listTetravex.at(i);
        listTetravex.at(i) = listTetravex.at(pos);
        listTetravex.at(pos) = temp;
    }
}

void TetravexSolver::swap(int firstX, int firstY, int secondX, int secondY)
{
    int i = firstX + firstY * own_size;
    int j = secondX + secondY * own_size;
    auto temp = listTetravex.at(i);
    listTetravex.at(i) = listTetravex.at(j);
    listTetravex.at(j) = temp;
}
/**
 *      0
 *    1  2
 *     3
 */
int TetravexSolver::computeDistance()
{
    int error = 0;
    /* Parcours les tetravex de la ligne courante **/
    for (int n_ligne = 0; n_ligne < own_size; n_ligne ++) {
        for (int i = 0; i < own_size; i++) { /** Pour chaque tetravex on check gauche et bas **/
            auto currentTetravex = listTetravex.at(i + n_ligne * own_size).getTetravex();
            /** On ne check pas gauche pour le 0 et dernier tetravex traite a part **/
            if (i < own_size - 1) {
                /* Check gauche droite **/
                auto nextTetravex = listTetravex.at(i + 1 + n_ligne * own_size).getTetravex();

                if (currentTetravex[2] != nextTetravex[1]) {
                    error++;
               //     std::cout << "Error found on left right line "  << n_ligne << " and column "<< i<< std::endl;
                }

            }
            if (n_ligne < own_size - 1) {
                /** Check l'en dessous sauf pour la derniere ligne **/
                auto underTetravex = listTetravex.at(i + (n_ligne + 1) * own_size).getTetravex();
                if (currentTetravex[3] != underTetravex[0]) {
                 //   std::cout << "Error found on up down at line " << n_ligne << " and column " << i << std::endl;
                    error++;
                }
            }

        }
    }
    return error;
}

/** Application de la méthode du recuit **/
void TetravexSolver::Solve(double Temperature)
{
    /** Initialisation **/

    double currentTemperature = Temperature;
    int currentDistance = computeDistance();
    int n_iteration = 0;
    int n_change = 0;
    int n_accepted = 0;
    bool changed = true;
    int nb_iter_tot = 0;

    /** Boucle principale du recuit **/
    while (true)
    {
        n_iteration++;
        nb_iter_tot++;
        /** Selection de 2 tetravex à echangé **/
        int posxFirst = rand() % own_size;
        int posyFirst = rand() % own_size;
        int posxSecond = rand() % own_size;
        int posySecond = rand() % own_size;
        while(true)
        {
            if (posxFirst != posxSecond || posyFirst != posySecond)
                break;
    /** SI ils sont les mêmes alors on boucle jusqu'a en prendre 2 séparés **/
                posxFirst = rand() % own_size;
                posyFirst = rand() % own_size;
                posxSecond = rand() % own_size;
                posySecond = rand() % own_size;
        }

        swap(posxFirst,posyFirst, posxSecond, posySecond);
        /** Verification si le changement est utile **/
        int new_dist = computeDistance();
        // Cas d'acceptation
        if (new_dist < currentDistance)
        {
            currentDistance = new_dist;
            changed = true;
            n_accepted ++;
        }
        else
        {
            /** Acceptation du changement avec une probabilité de plus en plus basse.
             * Objectif : Sortir de minimum locaux au début
             */
            double delta = -((new_dist - currentDistance) / currentTemperature);
            delta = exp(delta);
            double randomTest = rand() % 100000;
            /* Cas d'acceptation malgre proba */
            if (randomTest <= delta * 100000)
            {
                changed = true;
                currentDistance = new_dist;
                n_change ++;
            }
            else
            {
                /* Annulation swap et repositionnement des tetravex*/
                swap(posxSecond, posySecond, posxFirst, posyFirst);
            }
        }
        /** Condition de changement de pallier **/
        if (n_iteration >= 100 * own_size * own_size || n_accepted >= 12 * own_size * own_size)
        {
            currentTemperature = 0.9 * currentTemperature;
            n_iteration = 0;
            n_accepted = 0;
            if (changed)
                n_change = 0;
            else
                n_change++;
            changed = false;
            if (currentDistance == 0)
            {
           /** Solution trouvé, affichage et return **/
                printMap();
                return;
            }
        }
    }

}

void TetravexSolver::parseFile(std::string path)
{
    std::ifstream fichier(path, std::ios::in);  // on ouvre en lecture

    if(fichier)  // si l'ouverture a fonctionné
    {
        if(fichier)
        {
            std::string ligne;
            getline(fichier, ligne); // Considere un fichier ligne unique
            for (int i = 0; i < ligne.length(); i +=  5) {

                char nord = ligne[i];
                char est = ligne[i + 1];
                char sud = ligne[i + 2];
                char ouest = ligne[i + 3];
                /** Ma creation de tetravex est en sens inverse du format, j'adapte donc
                *      0
                *    1  2
                 *     3
                    */
                Tetravex Tetra;
                auto tetra = Tetra.getTetravex();
                tetra[0] = nord - '0';
                tetra[1] = ouest - '0';
                tetra[2] = est - '0';
                tetra[3] = sud - '0';


                listTetravex.push_back(Tetra);
            }
        }

    }
    else
        std::cerr << "Impossible d'ouvrir le fichier !" << std::endl;
    /** Set size **/
    own_size = (int)sqrt(listTetravex.size());
}