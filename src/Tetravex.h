//
// Created by benjamin angelard on 20/12/2017.
//

#ifndef TETRAVEX_TETRAVEX_H
#define TETRAVEX_TETRAVEX_H


class Tetravex {

public:
    /**
     *      0
     *    1  2
     *     3
     */
    Tetravex() = default;
    int* getTetravex() { return tetra ;}

private:
    int *tetra = new int[4];
};


#endif //TETRAVEX_TETRAVEX_H
