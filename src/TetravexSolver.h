//
// Created by benjamin angelard on 20/12/2017.
//

#ifndef TETRAVEX_TETRAVEXSOLVER_H
#define TETRAVEX_TETRAVEXSOLVER_H
#include "Tetravex.h"
#include <vector>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <fstream>
#include <iostream>
class TetravexSolver {
public:
    TetravexSolver() = default;
    void generateTetravex(int size);
    void fillTetravex(int size);
    void pretty_printMap();
    void printMap();
    void shuffle();
    void Solve(double Temperature);
    int computeDistance();
    void swap(int firstX, int firstY, int secondX, int secondY);
    void parseFile(std::string path);

private:
    std::vector<Tetravex> listTetravex = std::vector<Tetravex>();
    int own_size;
};


#endif //TETRAVEX_TETRAVEXSOLVER_H
