//
// Created by benjamin angelard on 20/12/2017.
//

#include "TetravexSolver.h"
// #define PRETTY_PRINT
#ifdef PRETTY_PRINT
#define LOG(msg) msg;
#else
#define LOG(msg)
#endif
int main(int argc, char ** argv)
{
    if (argc < 2)
    {
        std::cout << " Invalid number of parameters, need input_file in first argument " <<std::endl;
        return 0;
    }
    TetravexSolver solver2;
    solver2.parseFile(argv[1]);
    solver2.Solve(10000);


    /**
  ** DEBUG WITH AUTO GENERATION OF TETRAVEX **
  * LOG(std::cout << "Affichage tetravex vanilla " << std::endl)
   TetravexSolver solver;
   int size = atoi(argv[1]);
   solver.generateTetravex(size);
   LOG(solver.printMap(size))
   LOG(std::cout << " [TEST] Distance a blanc : " << solver.computeDistance() << std::endl)
   LOG(std::cout << "Affichage tetravex shuffled " << std::endl)
   solver.shuffle();
   LOG(solver.printMap(size))
   LOG(std::cout << " [TEST] Distance apres shuffle : " << solver.computeDistance() << std::endl)
   solver.Solve(10000);
   **/
}